#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

char *prog;

char buf[8192];

void
wc(int f, char *s)
{
	int i, n;
	int l, w, c;
	bool inword;

	l = w = c = 0;
	inword = false;

	while ((n = read(f, buf, sizeof(buf))) > 0) {
		for (i = 0; i < n; i++) {
			c++;
			if (buf[i] == '\n')
				l++;
			if (strchr(" \r\t\n\v", buf[i]))
				inword = false;
			else if(!inword) {
				w++;
				inword = true;
			}
		}

	}
	if (n < 0) {
		fprintf(stderr, "%s: read error: %s", prog, s);
		return;
	}
	printf("%d %d %d %s\n", l, w, c, s);
}

int
main(int argc, char *argv[])
{
	int f, i;

	prog = argv[0];

	if (argc == 1)
		wc(0, "");
	else for (i = 1; i < argc; i++) {
		f = open(argv[i], O_RDONLY);
		if (f < 0)
			fprintf(stderr, "%s: could not open %s", prog, argv[i]);
		else {
			wc(f, argv[i]);
			close(f);
		}
	}
	return 0;
}
