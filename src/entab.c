#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define TABWIDTH 8

char *prog;

char buf[8192];

int
main(int argc, char *argv[])
{
	int i, j, n;
	int col, spaces, numtabs, twidth;

	prog = argv[0];

	twidth = TABWIDTH;
	if (argc > 1) {
		twidth = atoi(argv[1]);
	}

	col = spaces = 0;
	while ((n = read(0, buf, sizeof(buf))) > 0) {
		for (i = 0; i < n; i++) {
			if (buf[i] == ' ') {
				spaces++;
				col++;
			} else {
				if (spaces == 1)
					printf(" ");
				else if (spaces > 1) {
					numtabs = col / twidth - (col - spaces) / twidth;
					for (j = 0; j < numtabs; j++)
						printf("\t");
					if (numtabs >= 1)
						spaces = col - (col / twidth) * twidth;
					for (j = 0; j < spaces; j++)
						printf(" ");
				}

				spaces = 0;
				printf("%c", buf[i]);
				col++;
				if (buf[i] == '\n')
					col = 0;
			}
		}

	}
	if (n < 0) {
		fprintf(stderr, "%s: read error", prog);
		exit(EXIT_FAILURE);
	}
	return 0;
}
