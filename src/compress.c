#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

char *prog;

char buf[8192];

int
main(int argc, char *argv[])
{
	int i, j, n, lastc;
	long nrep;

	prog = argv[0];
	
	lastc = nrep = 0;
	while ((n = read(0, buf, sizeof(buf))) > 0) {
		for (i = 0; i < n; i++) {
			if (lastc > 0 && lastc != buf[i]) {
				printf("%c%ld", lastc, nrep);
				nrep = 0;
			}
			for (j = i; j < n && buf[j] == buf[i]; j++)
				nrep++;
			lastc = buf[i];
			i = j - 1;
		}

	}
	if (n < 0) {
		fprintf(stderr, "%s: read error", prog);
		exit(EXIT_FAILURE);
	}
	return 0;
}
