.TL \" title
Doing something cool with ZFS
.AU \" author name
Serapheim Dimitropoulos
.AI \" author identification
sdimitro@delphix.com
\" Illinois Institute of Technology
.AB \" abstract
This is my abstract here and it is great. An excellent
abstract that only includes compliments of how good it
is. Indeed, it is the perfect abstract for every possible
situation. Any situation that I can imagine. Yes, that's
it. The perfect abstract ladies and gentlemen.
.AE \" abstrace end
.NH \" a chapter/main section title
Motivation
.LP \" title ends and paragraph starts
Here I'm just writing why do I even care about this
subject and I convince you all why it is so cool!

As I do the aforementioned action, I also throw some
history here and facts to make my awesome argument.
.NH
Design
.LP
Here I explan my awesome design, while \fBI use bold letters\fP
for parts that I want to emphasize and \fIitalics\fP for filenames
and locations.

I can also go to the next line without having to open a new
paragraph:
.br
\fBThis is the next line, but not another paragrah :-)\fP
.NH
Some Code Samples
.LP
Try putting some code samples here.
.de SAMPLE
.br
.nr saveIN \\n(.i   \" double the backslash when defining a macro
.RS
.nf
.nh
..
.de ESAMPLE
.hy
.fi
.RE
.in \\n[saveIN]u    \" 'u' means 'units': do not scale this number 
..

.SAMPLE
#include <stdio.h>

int
main()
{
	fprintf(stdout,"%s","Good morning, World");
	return 0;
}
.ESAMPLE
.NH
Evaluation
.LP
Try putting some graphs here and
explain them. Maybe dtrace output too
and pictures.
.NH
References
.LP
Try putting some references here
.SH \" subsection this time
Appendix
.LP
Put whatever here

